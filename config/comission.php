<?php

return [
    'currency_rate_api_url' => env('CURRENCY_RATE_API', 'https://developers.paysera.com/tasks/api/currency-exchange-rates'),
    'deposit_fee' => env('DEPOSIT_FEE', 0.03),
    'withdraw_business_fee' => env('WITHDRAW_BUSINESS_FEE', 0.5),
    'withdraw_private_fee' => env('WITHDRAW_PRIVATE_FEE', 0.3),
];
