# Lumen Task Backend

I tried write minimal code. The duration of this task is as follows:

```php
30 min => Thinking about how to implement the task
50 min => Coding
```

## Installation

1. clone this repo
2. composer install
3. run command `./vendor/bin/phpunit`

## Usage

-   run command `php artisan comission:calc input.csv`
-   you can edit `input.cvs` in root of repo

## Contact

Navid.Rajaie@gmail.com

## License

[MIT](https://choosealicense.com/licenses/mit/)
