<?php

namespace Tests;

use App\Services\ComissionService;

class ComissionTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_that_base_comission_calc()
    {
        $commissionService = new ComissionService();
        $this->assertEquals(
            0,
            $commissionService->calc(['2022-03-07', 4, 'private', 'withdraw', 200.00, 'EUR'])
        );
        $this->assertEquals(
            0,
            $commissionService->calc(['2022-03-07', 4, 'private', 'withdraw', 400.00, 'EUR'])
        );
        $this->assertEquals(
            0.3,
            $commissionService->calc(['2022-03-07', 4, 'private', 'withdraw', 500.00, 'EUR'])
        );
    }
}
