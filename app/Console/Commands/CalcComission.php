<?php

namespace App\Console\Commands;

use App\Services\ComissionService;
use Illuminate\Console\Command;

class CalcComission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'comission:calc {path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $comissionService = new ComissionService();
        $file = fopen($this->argument('path'), 'r');
        while (($line = fgetcsv($file)) !== FALSE) {
            $fee = $comissionService->calc($line);
            $this->info($fee);
        }
        fclose($file);
    }
}
