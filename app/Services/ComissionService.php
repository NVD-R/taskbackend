<?php

namespace App\Services;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Http;

class ComissionService
{
    public $rows;
    public $rates;
    public function __construct()
    {
        $this->rows = collect();
        $this->rates = Http::get(config('comission.currency_rate_api_url'))->json();
    }
    public function calc($data)
    {
        list($date, $userId, $userType, $opType, $opAmount, $opCurrency) = $data;

        switch ($opType) {
            case 'deposit':
                $fee = $this->deposit($opAmount);
                break;
            case 'withdraw':
                $fee = $this->withdraw($opAmount, $opCurrency, $userType, $userId);
                break;
            default:
                throw new Exception('Wrong Fromat');
                break;
        }
        $this->rows->push([
            'date' => $date,
            'user_id' => $userId,
            'user_type' => $userType,
            'op_type' => $opType,
            'op_amount' => $opAmount,
            'op_currency' => $opCurrency
        ]);
        return round($fee, 2);
    }

    private function deposit($opAmount)
    {
        return ($opAmount / 100) * (config('comission.deposit_fee'));
    }

    private function withdraw($opAmount, $opCurrency, $userType, $userId)
    {
        switch ($userType) {
            case 'business':
                return ($opAmount / 100) * config('comission.withdraw_business_fee');
                break;
            case 'private':
                return ($this->getAmountForFee($opCurrency, $userId, $opAmount) / 100) * config('comission.withdraw_private_fee');
                break;
            default:
                throw new Exception('Wrong Fromat');
                break;
        }
    }

    private function getAmountForFee($opCurrency, $userId, $opAmount)
    {
        $weekUserRows = $this->rows->where('user_id', $userId)->where('date', '>=',  Carbon::now()->startOfWeek()->format('Y-m-d'));
        if ($weekUserRows->count() < 4) {
            $amountSum = 0;
            foreach ($weekUserRows->toArray() as $row) {
                $amountSum += $row['op_amount'];
            };
            if (($discountLimitAmount = (1000 * $this->rates['rates'][$opCurrency]) - $amountSum) > 0) {
                return ($result = $opAmount - ($discountLimitAmount)) > 0 ? $result : 0;
            } else {
                return $opAmount;
            }
        }
        return $opAmount;
    }
}
